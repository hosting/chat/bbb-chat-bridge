# bbb-chat-bridge

This is a proof-of-concept for forwarding messages from a matrix chat room to a BigBlueButton meeting, and vice versa.

It is an independent node.js service (based on nest.js) which can be launched on any BigBlueButton server.

The app subscribes to BigBlueButton's Redis server and to the matrix server. It will listen to any incoming messages in the matrix chat room and forward them to the BigBlueButton meeting, as well as subscribe to the BigBlueButton chat messages and forward them to matrix.

![structure](doc/sequence.png)

## Architecture

For each chat backend, there is a service, providing the necessary means to send/receive chat messages. The connection between particular chat backends is made via a bridge service. 

In the current implementation which provides BigBlueButton - Matrix integration, the corresponding components are 

* `BbbService` (src/bbb/bbb.service.ts), 
* `MatrixService` (src/matrix/matrix.service.ts) and 
* `BbbMatrixBridge` (src/bridges/bbb-matrix/bbb-matrix.sevice.ts).
 
Every service takes care of listening for incoming chat messages and sending outgoing chat message to their respective backend. The Bridge is responsible for instantiating these services and providing the connector between them.

![architecture_bridge](doc/structure_bridge.png)

A new chat backend can be added by creating a service and a corresponding bridge.

## Installation

```bash
$ npm install
```

## Configuration

an .env file in the main project directory is required. It must contain the following values:

```shell
MATRIX_SERVER=<your matrix homeserver>
MATRIX_USER=<the matrix user that should be used>
MATRIX_PASSWORD=<password of the matrix user>
BRIDGE_COMMAND=<command that is used for bridging. default: /bridge>
```

You can find a file named .env-example in the main directory. To use it, adjust the values and change it's name to .env.

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Usage

When launched, the service will connect to the defined matrix server. To bridge a BBB meeting to a matrix room, 
run "/bridge start <room-id>" (/bridge can be changed via config; see above) in the public chat of your BBB meeting. All messages from the BBB
Meeting will now be forwarded to your defined matrix room, and vice versa. Bridging can als be stopped, paused and resumed.

### matrix room id

You can find the id of your matrix room in Element by navigating to the room settings, hitting advanced and looking at the field "Internal room ID".

### Commands:

all commands are started with /bridge (or your user-defined string). For example "/bridge start".

| command | function                                             |
|---------|------------------------------------------------------|
| start   | starts the bridge (takes a room id as parameter)     |
| stop    | stops the bridge. all data on the bridge is deleted. |
| pause   | temporarily disables the bridge                      |
| resume  | resumes from pause                                   |