import {Injectable} from '@nestjs/common';
import {RunConfigService} from "../run-config/run-config.service";
import {ChatMessageBody} from "../chat/ChatMessageBody";
import {RedisService} from "./redis.service";

@Injectable()
export class BbbService {
    constructor(
        private redis: RedisService,
        private rcf: RunConfigService
    ) {
    }

    sendMessage(pMsg: ChatMessageBody) {
        return this.redis.sendMsg(pMsg);
    }

    buildMessage(pMsg, pMeeting, pUser): ChatMessageBody {
        // if (!this.rcf.BbbMeetingConfig || !this.rcf.BbbMeetingConfig.meetingId || !this.rcf.BbbMeetingConfig.userId) {
        //     console.error("no meetingId or userId configured yet. type /register in the bbb meeting that you'd like to " +
        //         "register");
        //     return;
        // }
        return <ChatMessageBody>{
            meetingId: pMeeting,
            userId: pUser,
            username: "",
            msg: pMsg,
            color: "#000"
        }
    }
}
