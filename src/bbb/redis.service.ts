import {Injectable} from '@nestjs/common';
import {createClient, RedisClientType, RedisDefaultModules, RedisModules, RedisScripts} from "redis";
import {RedisConfig} from "./interfaces/RedisConfig.interface";
import {RedisChatMessage} from "./interfaces/RedisChatMessage.interface";
import {ChatMessageBody} from "../chat/ChatMessageBody";
import {RunConfigService} from "../run-config/run-config.service";
import {ParsedMessage} from "./interfaces/ParsedMessage.interface";
import {ConfigService} from "@nestjs/config";

@Injectable()
export class RedisService {
    publisher: RedisClientType<RedisDefaultModules & RedisModules, RedisScripts> = null;
    subscriber: RedisClientType<RedisDefaultModules & RedisModules, RedisScripts> = null;
    config: RedisConfig;

    constructor(
        private rcf: RunConfigService,
        private configService: ConfigService

    ) {
        this.publisher = createClient();
        this.subscriber = this.publisher.duplicate();
        this.publisher.connect();
        this.subscriber.connect();

        this.config = {
            channel: "to-akka-apps-redis-channel",
            chatId: "MAIN-PUBLIC-GROUP-CHAT",
            eventName: "SendGroupChatMessageMsg"
        }
    }

    private __buildMessage(pBody: ChatMessageBody): RedisChatMessage {
        let tTimestamp = Date.now();
        let tMessage: RedisChatMessage = {
            envelope: {
                name: this.config.eventName,
                routing: {
                    meetingId: pBody.meetingId,
                    userId: pBody.userId
                },
                timestamp: tTimestamp,
            },
            core: {
                header: {
                    name: this.config.eventName,
                    meetingId: pBody.meetingId,
                    userId: pBody.userId
                },
                body: {
                    msg: {
                        chatEmphasizedText: true,
                        correlationId: pBody.userId + "-" + tTimestamp,
                        sender: {id: pBody.userId, name: "bbb-chat-bridge", role: ""},
                        message: pBody.msg
                    },
                    chatId: this.config.chatId,
                }
            }
        };
        return tMessage;
    }


    private __parseMessage(pMessage: string): ParsedMessage {
        let tObj = JSON.parse(pMessage);
        if (
            tObj.hasOwnProperty("envelope")
            && tObj.envelope.hasOwnProperty("name")
            && tObj.envelope.name === "SendGroupChatMessageMsg"
            && tObj.hasOwnProperty("core")
            && tObj.core.hasOwnProperty("header")
            && tObj.core.header.hasOwnProperty("name")
            && tObj.core.header.hasOwnProperty("meetingId")
            && tObj.core.header.hasOwnProperty("userId")
            && tObj.core.header.name === "SendGroupChatMessageMsg"
            && tObj.core.hasOwnProperty("body")
            && tObj.core.body.hasOwnProperty("msg")
        ) {
            return <ParsedMessage>{
                body: tObj.core.body.msg,
                meetingId: tObj.core.header.meetingId,
                userId: tObj.core.header.userId
            };
        }
    }

    async sendMsg(pMsg: ChatMessageBody) {
        const tMsg: RedisChatMessage = this.__buildMessage(pMsg);
        let a = await this.publisher.publish(this.config.channel, JSON.stringify(tMsg));
        return a;
    }

    createChatSubscription(pCallback) {
        return this.subscriber.subscribe(this.config.channel, async (message) => {
            let tP = this.__parseMessage(message);
            if(tP) {
                pCallback(tP);
            }
        });
    }

}

