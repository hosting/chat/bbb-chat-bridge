import {IsNotEmpty} from "class-validator";

export class ChatMessageBody {
    
    @IsNotEmpty()
    meetingId: string;

    @IsNotEmpty()
    userId: string;

    @IsNotEmpty()
    username: string;
    
    @IsNotEmpty()
    msg: string;
    
    color: string;
}