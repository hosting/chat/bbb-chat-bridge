import {Module} from '@nestjs/common';
import {ChatController} from './chat/chat.controller';
import { MatrixService } from './matrix/matrix.service';
import { RunConfigService } from './run-config/run-config.service';
import {ConfigModule} from "@nestjs/config";
import { BbbService } from './bbb/bbb.service';
import {RedisService} from "./bbb/redis.service";
import {BbbMatrixBridge} from "./bridges/bbb-matrix/bbb-matrix.sevice";

@Module({
    imports: [ConfigModule.forRoot()],
    controllers: [ChatController],
    providers: [
        RunConfigService,
        RedisService,
        BbbService,
        MatrixService,
        BbbMatrixBridge,
    ],
})
export class AppModule {
    constructor(
        ) {
    }
}
