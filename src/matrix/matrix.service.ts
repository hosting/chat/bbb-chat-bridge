import {Injectable} from '@nestjs/common';
import * as sdk from "matrix-js-sdk";
import {ConfigService} from "@nestjs/config";
import {BbbService} from "../bbb/bbb.service";

const matrixcs = require("matrix-js-sdk/lib/matrix");
const request = require("request");
matrixcs.request(request);

@Injectable()
export class MatrixService  {
    client: any;

    constructor(
        private readonly Bbb: BbbService,
        private configService: ConfigService
    ) {

    }

    parseUsername(tUser: string): string {
        return (tUser.split(":"))[0].substring(1) || "";
    }
    
    

    async connect() {
        try {
            this.client = sdk.createClient(this.configService.get("MATRIX_SERVER"));
            let rooms = await this.client.getRooms();
            let loginResponse = await this.client.login("m.login.password", {
                "user": this.configService.get("MATRIX_USER"),
                "password": this.configService.get("MATRIX_PASSWORD")
            });
            await this.client.startClient();
        } catch (e) {
            console.error(`#@# error: ${e}`);
        }
    }

    subscribeToRoomEvents(pCallback) {
        try {
            let syncState = this.client.once('sync', async (state, prevState, res) => {
                // @ts-ignore
                this.client.on("Room.timeline", pCallback);
            });
        } catch (e) {
            console.error(`#@# error: ${e}`);
        }
    }
    
    sendMessage(pMsg, pRoomId?) {

        var content = {
            "body": pMsg,
            "msgtype": "m.text",
            "origin": "api" 
        };

        this.client.sendEvent(pRoomId, "m.room.message", content, "").then((res) => {
            // message sent successfully
        }).catch((err) => {
            console.log(err);
        });
    }



}
