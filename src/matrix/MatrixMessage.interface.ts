
export interface MatrixMessage {
    sender: string;
    content: string;
    origin: string;
}