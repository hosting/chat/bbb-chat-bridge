export interface IBbbMatrixBridgeState {
    isActive: boolean,
    matrixRoom: string,
    bbbMeeting: string,
    bbbUserId: string
}