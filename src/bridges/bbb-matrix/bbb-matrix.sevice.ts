import {Injectable, OnModuleInit} from '@nestjs/common';
import {MatrixService} from "../../matrix/matrix.service";
import {ConfigService} from "@nestjs/config";
import {RedisService} from "../../bbb/redis.service";
import {ParsedMessage} from "../../bbb/interfaces/ParsedMessage.interface";
import {MatrixMessage} from "../../matrix/MatrixMessage.interface";
import {Subject} from "rxjs";
import {RunConfigService} from "../../run-config/run-config.service";
import {BbbService} from "../../bbb/bbb.service";
import {BbbMeetingConfig} from "../../bbb/interfaces/BbbMeetingConfig.interface";
import {IBbbMatrixBridgeState} from "./IBbbMatrixBridgeState.interface";

@Injectable()
export class BbbMatrixBridge implements OnModuleInit {

    bridgeState: IBbbMatrixBridgeState = {
        bbbMeeting: "", bbbUserId: "", isActive: false, matrixRoom: ""
    }

    bridgeRegistrationInitiated: Subject<any> = new Subject<any>();
    bridgeRegistrationCompleted: Subject<any> = new Subject<any>();

    commandEventListener: Promise<void>;
    bbbMeetingRegistered: Subject<boolean> = new Subject<boolean>();
    bbbMessageReceived: Subject<ParsedMessage> = new Subject<ParsedMessage>();
    matrixMessageReceived: Subject<MatrixMessage> = new Subject<MatrixMessage>();

    constructor(
        private readonly Bbb: BbbService,
        private readonly redis: RedisService,
        private readonly matrix: MatrixService,
        private configService: ConfigService,
        private rcf: RunConfigService
    ) {
    }

    config = {
        defaultBridgeCommand: "/bridge"
    }

    private __isBridgeDefined() {
        return this.bridgeState.hasOwnProperty("matrixRoom")
            && this.bridgeState.matrixRoom !== ""
            && this.bridgeState.hasOwnProperty("bbbMeeting")
            && this.bridgeState.bbbMeeting !== ""
            && this.bridgeState.hasOwnProperty("bbbUserId")
            && this.bridgeState.bbbUserId !== ""
    }

    private __getBridgeCommand() {
        return this.configService.get("BRIDGE_COMMAND") || this.config.defaultBridgeCommand || "/bridge";
    }

    private __isCommand(pMessage: ParsedMessage) {
        let tCommand = this.__getBridgeCommand();
        return pMessage.body.message.substring(0, tCommand.length) === tCommand
    }

    private __parseCommand(pMessage: ParsedMessage): string[] {
        let tAr = [];
        let tCommand = this.__getBridgeCommand();
        if (
            pMessage.hasOwnProperty("body")
            && pMessage.body.hasOwnProperty("message")
            && pMessage.body.message.substring(0, tCommand.length) === tCommand
        ) {
            tAr = pMessage.body.message.split(" ");
        }
        return tAr;
    }

    handleIncomingMatrixEvent = (event, room) => {
        if (room.roomId !== this.bridgeState.matrixRoom || event.getType() !== "m.room.message") {
            return; // sort out all irrelevant events
        }

        let tMsg: MatrixMessage = {
            sender: event.getSender(),
            content: event.event.content.body,
            origin: event.event.content.origin
        }
        if (event.event && event.event.content && event.event.content.origin) tMsg.origin = event.event.content.origin

        this.matrixMessageReceived.next(tMsg);
    }

    forwardMessageToMatrix = (val: ParsedMessage) => {
        if (val.body.sender.name !== "bbb-chat-bridge") {
            this.matrix.sendMessage(`${val.body.sender.name}: ${val.body.message}`, this.bridgeState.matrixRoom);
        }
    }

    forwardMessageToBbb = (message: MatrixMessage) => {
        if (
            !this.bridgeState.hasOwnProperty("bbbMeeting")
            || !this.bridgeState.hasOwnProperty("bbbUserId")
        ) {
            console.error(`no meetingId or userId configured yet. type ${this.__getBridgeCommand()} in the bbb meeting that you'd like to register`)
            return;
        }
        if (message.origin !== "api") {
            let tMsg = this.Bbb.buildMessage(
                `${message.sender ? (this.matrix.parseUsername(message.sender) + ": ") : ""} ${message.content}`,
                this.bridgeState.bbbMeeting,
                this.bridgeState.bbbUserId,
            );
            if (tMsg) this.Bbb.sendMessage(tMsg);
        }
    }

    bbbMessageHandler = (pMessage) => {
        if (this.__isCommand(pMessage)) {
            let tCommand = (pMessage) ? this.__parseCommand(pMessage) : [];
            if (tCommand.length > 1) {
                switch (tCommand[1]) {
                    case "start":
                        let tRoom = tCommand[2];

                        this.bridgeState = {
                            isActive: true,
                            matrixRoom: tRoom,
                            bbbMeeting: pMessage.meetingId,
                            bbbUserId: pMessage.userId
                        }
                        console.log(`started bridge to ${tRoom}`);
                        break;
                    case "pause":
                        if (this.__isBridgeDefined()) {
                            this.bridgeState.isActive = false;
                            console.log(`paused bridge`);
                        } else {
                            console.error("no bridge defined")
                        }
                        break;
                    case "resume":
                        if (this.__isBridgeDefined()) {
                            this.bridgeState.isActive = true;
                            console.log(`resumed bridge`);
                        } else {
                            console.error("no bridge defined")
                        }
                        break;
                    case "stop":
                        this.stopBridge();
                        break;
                }
            }
        } else {
            if (this.bridgeState.hasOwnProperty("isActive") && this.bridgeState.isActive) {
                this.bbbMessageReceived.next(pMessage);
            } else {
                console.warn("Bridge not active")
            }
        }
    }

    stopBridge() {
        this.bridgeState = {
            bbbMeeting: "", bbbUserId: "", isActive: false, matrixRoom: ""
        }
        console.log("stopped bridge");
    }

    async activate() {

        // connect matrix client
        await this.matrix.connect();

        // listen to a registration event
        await this.redis.createChatSubscription(this.bbbMessageHandler);

        // matrix event incoming
        await this.matrix.subscribeToRoomEvents(this.handleIncomingMatrixEvent);

        // if message is received in matrix, forward the message to bbb
        this.matrixMessageReceived.subscribe(this.forwardMessageToBbb);

        // if message is received in bbb, forward the message to matrix
        this.bbbMessageReceived.subscribe(this.forwardMessageToMatrix);
        console.log("BBB-Matrix bridge active");
    }


    async onModuleInit(): Promise<void> {
        await this.activate();
    }
}
