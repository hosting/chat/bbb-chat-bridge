import { Injectable } from '@nestjs/common';
import {BbbMeetingConfig} from "../bbb/interfaces/BbbMeetingConfig.interface";

@Injectable()
export class RunConfigService {
    BbbMeetingConfig: BbbMeetingConfig;
    
    updateBbbMeetingConfig(pConfig: BbbMeetingConfig) {
        this.BbbMeetingConfig = pConfig;
    }
}
